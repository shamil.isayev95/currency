import CurrencyService from '../../services/currencyService';
import * as types from '../types';

export const setCurrencyFetching = isFetching => ({
    type: types.CURRENCY_FETCHING_CHANGED,
    payload: isFetching,
});

export const setCurrency = currency => ({
    type: types.CURRENCY_CHANGED,
    payload: currency,
});

export const setCurrentCurrency = currency => ({
    type: types.CURRENT_CURRENCY_CHANGED,
    payload: currency,
});

export const getCurrency = () => async dispatch => {
    dispatch(setCurrencyFetching(true));

    const response = await CurrencyService.getCurrency();

    if (!Object.keys(response).length) {
        dispatch(setCurrencyFetching(false));
        return;
    }

    dispatch(setCurrency(response));
    dispatch(setCurrencyFetching(false));

    return response;
};
