export const CURRENCY_CHANGED = 'currency/changed';
export const CURRENCY_FETCHING_CHANGED = 'currency-fetching/changed';
export const CURRENT_CURRENCY_CHANGED = 'current-currency/changed';
