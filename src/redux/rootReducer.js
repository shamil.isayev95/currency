import { combineReducers } from 'redux';
import currency from './reducers/currency';

const rootReducer = combineReducers({
    currency,
});

export default rootReducer;
