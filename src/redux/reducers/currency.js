import * as types from '../types';
import { CURRENCY_FETCHING_CHANGED, CURRENT_CURRENCY_CHANGED } from '../types';
import { CURRENCIES } from '../../constants';

const initialState = {
    currencies: {},
    isFetching: false,
    currentCurrency: CURRENCIES[0].value,
};

const currency = (state = initialState, action) => {
    const { payload } = action;

    switch (action.type) {
        case types.CURRENCY_CHANGED: {
            return {
                ...state,
                currencies: payload,
            };
        }
        case CURRENCY_FETCHING_CHANGED: {
            return {
                ...state,
                isFetching: payload,
            };
        }
        case CURRENT_CURRENCY_CHANGED: {
            return {
                ...state,
                currentCurrency: payload,
            };
        }
        default:
            return state;
    }
};

export default currency;
