import classnames from 'classnames';
import PropTypes from 'prop-types';

import styles from './Spin.module.scss';

const Spin = ({ className, spinning }) => {
    return (
        <div className={classnames('spin', styles.container, className)}>
            <span className={classnames(styles.spinElement, { [styles.spinning]: spinning })}></span>
        </div>
    );
};

Spin.displayName = '@shared/Spin';

Spin.defaultProps = {
    spinning: false,
};

Spin.propTypes = {
    className: PropTypes.string,
    spinning: PropTypes.bool.isRequired,
};

export default Spin;
