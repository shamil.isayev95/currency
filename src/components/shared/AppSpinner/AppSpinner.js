import classNames from 'classnames';
import PropTypes from 'prop-types';
import Spin from '../Spin';

import styles from './AppSpinner.module.scss';

const AppSpinner = props => {
    const { spinning } = props;

    return (
        <div className={classNames(styles.overlay, { spinning })}>
            <Spin spinning={spinning} />
        </div>
    );
};

AppSpinner.displayName = '@shared/Spin';

AppSpinner.defaultProps = {
    spinning: false,
};

AppSpinner.propTypes = {
    className: PropTypes.string,
    spinning: PropTypes.bool.isRequired,
};

export default AppSpinner;
