import React, { useEffect, useMemo, useState } from 'react';
import { getCurrency } from '../../redux/actions/currency';
import { useDispatch, useSelector } from 'react-redux';
import AppSpinner from '../shared/AppSpinner';
import Divider from '../Divider';
import Filters from './Filters/Filters';
import CoinCurrency from './CoinCurrency/CoinCurrency';

import styles from './Currency.module.scss';

const Currency = () => {
    const dispatch = useDispatch();
    const currency = useSelector(state => state.currency.currencies);
    const currentCurrency = useSelector(state => state.currency.currentCurrency);

    useEffect(() => {
        dispatch(getCurrency());
    }, []);

    useEffect(() => {
        const timer = setInterval(() => {
            dispatch(getCurrency());
        }, 10000);

        return () => clearInterval(timer);
    }, []);

    const displayedCurrency = useMemo(() => {
        return currency.bpi && currency.bpi[currentCurrency];
    }, [currentCurrency, currency]);

    return !Object.keys(currency).length ? (
        <AppSpinner spinning={currency.isFetching} />
    ) : (
        <div className={styles.container}>
            <div className={styles.currencyContainer}>
                <div className={styles.header}>
                    <span>Time: </span>
                    <span>{currency.time.updated}</span>
                </div>
                <Divider className={styles.headerDivider} orientation={'horizontal'} />
                <Filters />
                <CoinCurrency currency={displayedCurrency} />
                <Divider className={styles.headerDivider} orientation={'horizontal'} />
                <div>{currency.disclaimer}</div>
            </div>
        </div>
    );
};

export default Currency;
