import Select from 'react-select';
import { CURRENCIES } from '../../../constants';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentCurrency } from '../../../redux/actions/currency';

import styles from './Filters.module.scss';

const Filters = () => {
    const dispatch = useDispatch();
    const currentCurrency = useSelector(state => state.currency.currentCurrency);

    const onCurrencyChange = value => {
        dispatch(setCurrentCurrency(value));
    };
    return (
        <div>
            <Select
                className={styles.currency}
                options={CURRENCIES}
                onChange={({ value }) => onCurrencyChange(value)}
                defaultValue={CURRENCIES[0]}
            />
        </div>
    );
};

export default Filters;
