import styles from '../Currency.module.scss';

const CoinCurrency = props => {
    const { currency } = props;

    return (
        <>
            <div className={styles.currentCurrency}>
                <span>{currency?.description}: </span>
                <span>
                    {currency?.rate_float} {currency?.code}
                </span>
            </div>
            <div className={styles.currentCurrency}>
                <span>Rate: </span>
                <span>
                    {currency.rate} {currency?.code}
                </span>
            </div>
        </>
    );
};

export default CoinCurrency;
