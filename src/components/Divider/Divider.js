import classNames from 'classnames';
import PropTypes from 'prop-types';

import styles from './Divider.module.scss';

const DIVIDER_ORIENTATIONS = {
    horizontal: 'horizontal',
    vertical: 'vertical',
};

const STYLES = {
    [DIVIDER_ORIENTATIONS.horizontal]: styles.horizontal,
    [DIVIDER_ORIENTATIONS.vertical]: styles.vertical,
};

const Divider = props => {
    const { className, orientation } = props;

    return <div className={classNames(STYLES[orientation], className)} />;
};

Divider.propTypes = {
    className: PropTypes.string,
    orientation: PropTypes.oneOf(Object.values(DIVIDER_ORIENTATIONS)).isRequired,
};

export default Divider;
