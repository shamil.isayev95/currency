import { CURRENCY_API_ENDPOINT } from '../../config';
import axios from 'axios';

const getCurrency = async () => {
    try {
        const result = await axios.get(`${CURRENCY_API_ENDPOINT}/currentprice.json`);

        return result.data;
    } catch (err) {
        console.log(err);
    }
};

const CurrencyService = {
    getCurrency,
};

export default CurrencyService;
