const getServiceUrls = () => {
    const currencyService = `${process.env.NEXT_PUBLIC_COINDESK_URL}/bpi`;

    return {
        CURRENCY_API_ENDPOINT: currencyService,
    };
};

export const { CURRENCY_API_ENDPOINT } = getServiceUrls();
